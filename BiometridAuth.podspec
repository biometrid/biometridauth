Pod::Spec.new do |s|  
    s.name              = 'BiometridAuth'
    s.version           = '2.2.2'
    s.summary           = 'Biometrid Auth is a service-based multimodal biometric authentication and document validation platform'
    s.homepage          = 'https://polygon.pt/'

    s.author            = { 'Name' => 'Polygon' }
    s.license           = { :type => 'MIT', :file => 'LICENSE' }

   s.platform          = :ios
s.source					= { :git => 'https://bitbucket.org/polygon_/biometridauth/src/', :tag => 'bio.auth.2.2.2' }
	s.ios.vendored_frameworks 	= 'BiometridAuth.framework', 'ZoomWrapperSDK.framework', 'ZoomAuthentication.framework'
s.pod_target_xcconfig 		= { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
  	s.user_target_xcconfig 		= { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }

s.ios.deployment_target = '12.0'
s.ios.vendored_frameworks = 'BiometridAuth.framework', 'ZoomAuthentication.framework'

end  
