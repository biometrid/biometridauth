//
//  ZoomAuthentication.h
//  ZoomAuthentication
//
//  Copyright © 2019 FaceTec. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for ZoomAuthentication.
FOUNDATION_EXPORT double ZoomAuthenticationVersionNumber;

//! Project version string for ZoomAuthentication.
FOUNDATION_EXPORT const unsigned char ZoomAuthenticationVersionString[];

#import <ZoomAuthentication/ZoomPublicApi.h>
