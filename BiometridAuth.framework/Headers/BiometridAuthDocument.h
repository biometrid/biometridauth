//
//  BiometridAuthDocument.h
//  BiometridAuth
//
//  Created by Tiago Carvalho on 07/02/2018.
//  Copyright © 2018 Tiago Carvalho. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <BiometridAuth/BiometridAuth.h>
#import <BiometridAuth/BiometridAuthFaceResponse.h>

#define DOCUMENT_HANDLER (void(^)(NSDictionary *response, BOOL processed, NSError *error))
#define SELFIE_COMPARE_HANDLER (void(^)(NSDictionary *response, BiometridAuthFaceResponse *baFaceResponse, NSError *error))

typedef enum DOCUMENT_TYPE {
    IDCARD,
    PASSPORT
} DOCUMENT_TYPE;

@interface BiometridAuthDocument : NSObject <NSURLSessionDelegate, NSURLSessionTaskDelegate>

+ (instancetype)sharedInstance;

/*!
 *  @discussion Document engine, validate documents.
 *  @param doctype The type of the document to validate.
 *  @param frontImagePath Path of the file with the front image of the document.
 *  @param backImagePath Path of the file with the back image of the document.
 */
- (void)documentValidate:(DOCUMENT_TYPE)doctype frontImage:(NSString*)frontImagePath backImage:(NSString*)backImagePath andCallback: DOCUMENT_HANDLER completion;

/*!
 *  @discussion Document engine, validate documents.
 *  @param doctype The type of the document to validate.
 *  @param frontBase64Image Base64 of the file with the front image of the document.
 *  @param backBase64Image Base64 of the file with the back image of the document.
 */
- (void)documentBase64Validate:(DOCUMENT_TYPE)doctype frontImage:(NSString*)frontBase64Image backImage:(NSString*)backBase64Image andCallback: DOCUMENT_HANDLER completion;

/*!
 *  @discussion Document engine, selfie compare. Selfie used here is from liveness.
 *  @param filePath File path of the document id to be compared with selfie liveness.
 */
- (void)documentSelfieCompare:(NSString*)filePath andCallback: SELFIE_COMPARE_HANDLER completion;

/*!
 *  @discussion Document engine, selfie compare. Selfie used here is from liveness.
 *  @param fileBase64 File in base64 of the document id to be compared with selfie liveness.
 */
- (void)documentSelfieBase64Compare:(NSString*)fileBase64 andCallback: SELFIE_COMPARE_HANDLER completion;

/*!
 *  @discussion Document engine, selfie compare.
 *  @param filePath1 File path of the document id to be compared with selfie liveness.
 *  @param filePath2 File path of the document id to be compared with selfie liveness.
 */
- (void)documentSelfieCompare:(NSString*)filePath1 andFilePath:(NSString*)filePath2 andCallback: SELFIE_COMPARE_HANDLER completion;

/*!
 *  @discussion Document engine, selfie compare.
 *  @param file1Base64 File in base64 of the document id to be compared with selfie liveness.
 *  @param file2Base64 File in base64 of the document id to be compared with selfie liveness.
 */
- (void)documentSelfieBase64Compare:(NSString*)file1Base64 andFileBase64:(NSString*)file2Base64 andCallback: SELFIE_COMPARE_HANDLER completion;

@end
