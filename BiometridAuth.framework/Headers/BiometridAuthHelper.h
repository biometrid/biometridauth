//
//  BiometridAuthHelper.h
//  BiometridAuth
//
//  Created by Tiago Carvalho on 29/12/2017.
//  Copyright © 2017 Tiago Carvalho. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>

@interface BiometridAuthHelper : NSObject <AVAudioRecorderDelegate>

@property NSString *macAddress;

+ (instancetype)sharedInstance;


// Voice engine helpers

/*!
 *  @discussion Get the record settings required for the audio file.
 *  @return Record settings.
 */
- (NSDictionary*)getRecordSettings;

/*!
 *  @discussion Start recording the audio file.
 *  @param seconds Duration of the recording (default is 4).
 *  @param delegate AVAudioRecorderDelegate delegate.
 *  @return File path where audio is saved.
 */
- (NSString*)startRecording:(NSInteger)seconds withDelegate:(id<AVAudioRecorderDelegate>)delegate;

/*!
 *  @discussion Cancel the recording of the audio file.
 */
- (void)cancelRecording;

// Face engine helpers

/*!
 *  @discussion Save a file and return its path from an image data, also formats the image with the required settings.
 *  @param photoData Captured photo data.
 *  @return File path where photo is saved.
 */
- (NSString*)saveFileFromData:(NSData *)photoData;

// create unique identifier for device
- (NSString*)getMacAddress;


@end


