//
//  FaceResponse.h
//  BiometridAuth
//
//  Created by Tiago Carvalho on 14/06/2018.
//  Copyright © 2018 Tiago Carvalho. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <BiometridAuth/BiometridAuthEnums.h>

@interface BiometridAuthFaceResponse : NSObject

@property FaceResponseStatus faceResponseStatus;
@property NSString* userId;
@property NSString* faceImage;
@property BOOL liveness;
@property float confidence;
@property NSError *error;

+ (instancetype)faceResponseWithStatus:(FaceResponseStatus)faceResponseStatus andError:(NSError*)error;

+ (instancetype)faceResponseWithStatus:(FaceResponseStatus)faceResponseStatus withFaceImage:(NSString*)faceImage withUserId:(NSString*)userId withLiveness:(BOOL)liveness withConfidence:(float)confidence andError:(NSError*)error;

@end

