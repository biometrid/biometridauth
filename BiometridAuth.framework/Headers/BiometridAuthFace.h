//
//  BiometridAuthFace.h
//  BiometridAuth
//
//  Created by Tiago Carvalho on 07/02/2018.
//  Copyright © 2018 Tiago Carvalho. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <BiometridAuth/BiometridAuth.h>
#import <BiometridAuth/BiometridAuthFaceResponse.h>
#import <BiometridAuth/BiometridAuthLivenessCustomization.h>

@protocol BiometridAuthFaceCallback <NSObject>
@optional
- (void)started;
- (void)stopped;
@end

@interface BiometridAuthFace : NSObject <NSURLSessionDelegate, NSURLSessionTaskDelegate>
@property NSString *livenessId;
 
+ (instancetype)sharedInstance;

- (void)setBiometridAuthFaceCallback:(id <BiometridAuthFaceCallback>)callback;

/*!
 *  @discussion Face engine.
 *  @param customLayout of the Controller.
 *  @param sessionType Session type.
 *  @param userId Biometrid's user identifier (send nil to get userId from photo).
 *  @param filePath File path of the photo to be submitted (send nil to start liveness process).
 *  @param viewController View controller where the face liveness engine is going to be launched.
 */
- (void)faceEngine:(BiometridAuthLivenessCustomization*)customLayout withSessionType:(SessionType)sessionType user:(NSString*)userId withFilePath:(NSString*)filePath inViewController:(UIViewController*)viewController andCallback:(void(^)(NSDictionary *response, BiometridAuthFaceResponse *baFaceResponse, LivenessResultStatus livenessResultStatus, NSError *error))completion;

/*!
 *  @discussion Get face engine's liveness status in the BO.
 *  @param userId Biometrid's user identifier.
 */
- (void)getLivenessStatus:(NSString*)userId andCallback:(void(^)(NSDictionary *response, BOOL hasLiveness, NSError *error))completion;

/*!
 *  @discussion Get face emotions properties.
 *  @param filePath File path of the photo to be submitted (send nil to start liveness process).
 */
- (void)findFaceProperties:(NSString*)filePath andCallback:(void(^)(NSDictionary *response, NSError *error))completion;

/*!
*  @discussion Get face emotions properties.
 *  @param customLayout of the Controller.
 *  @param viewController View controller where the face liveness engine is going to be launched.
*/
- (void)getLivenessPhoto:(BiometridAuthLivenessCustomization*)customLayout inViewController:(UIViewController*)viewController andCallback:(void(^)(NSDictionary *response, BiometridAuthFaceResponse *baFaceResponse, LivenessResultStatus livenessResultStatus, NSError *error))completion;

@end
