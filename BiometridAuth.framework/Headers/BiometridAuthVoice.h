//
//  BiometridAuthVoice.h
//  BiometridAuth
//
//  Created by Tiago Carvalho on 07/02/2018.
//  Copyright © 2018 Tiago Carvalho. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <BiometridAuth/BiometridAuth.h>
#import <BiometridAuth/BiometridAuthVoiceResponse.h>

#define DOCUMENTS_FOLDER [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"]
#define VOICE_HANDLER (void(^)(NSDictionary *response, BiometridAuthVoiceResponse *baVoiceResponse, NSError *error))

@interface BiometridAuthVoice : NSObject <NSURLSessionDelegate, NSURLSessionTaskDelegate>

+ (instancetype)sharedInstance;

/*!
 *  @param sessionType Session type.
 *  @param userId Biometrid's user identifier.
 *  @param language Biometrid's user language (ENGLISH / SPANISH).
 */
- (void) startRecording:(SessionType)sessionType user:(NSString*)userId withLanguage:(Language)language andCompletion: VOICE_HANDLER completion;

- (void) stopRecording;

- (BOOL) isRecording;

/*!
 *  @discussion Voice engine.
 *  @param sessionType Session type.
 *  @param userId Biometrid's user identifier.
 *  @param language Biometrid's user language (ENGLISH / SPANISH).
 *  @param filePath File path of the audio file to be submitted (send nil to start a new dialogue and get a prompt).
 */
- (void)voiceEngine:(SessionType)sessionType user:(NSString*)userId withLanguage:(Language)language withFilePath:(NSString*)filePath andCallback: VOICE_HANDLER completion;

@end

