//
//  BiometridAuthSDK.h
//  BiometridAuth
//
//  Created by Tiago Carvalho on 29/12/2017.
//  Copyright © 2017 Tiago Carvalho. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface BiometridAuthSDK : NSObject <NSURLSessionDelegate, NSURLSessionTaskDelegate>

+ (instancetype)sharedInstance;

/*!
 *  @discussion BiometridAuth SDK initialization with credentials.
 *  @param apiKey Application client key.
 *  @param apiSecret Application client secret.
 *  @param serverUrl Application server url.
 *  @warning init must be called at least once by the application before invoking any SDK operations.
 */
- (void)initWithApiKey:(NSString*)apiKey withApiSecret:(NSString*)apiSecret withServerUrl:(NSString*)serverUrl andCallback:(void(^)(NSDictionary *response, BOOL isValid, NSError *error))completion;


@end
