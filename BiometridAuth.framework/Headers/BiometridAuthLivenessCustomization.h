//
//  BiometridAuthLivenessCustomization.h
//  BiometridAuth
//
//  Created by André Santana on 18/10/2019.
//  Copyright © 2019 André Santana. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <BiometridAuth/BiometridAuthEnums.h>

@interface BiometridAuthLivenessCustomization : NSObject

/*!
 *  @discussion Custom branding logo.
 *  @param image of brandingLogo.
 */
- (void)setBrandingLogo:(UIImage*)image;

/*!
 *  @discussion Progress color one.
 *  @param color option.
 */
- (void)setOvalCustomizationProgressColorOne:(UIColor*)color;

/*!
 *  @discussion Progress color two.
 *  @param color option.
 */
- (void)setOvalCustomizationProgressColorTwo:(UIColor*)color;

/*!
 *  @discussion Feedback background color.
 *  @param color of background color.
 */
- (void)setFeedbackCustomizationBackgroundColor:(UIColor*)color;

/*!
 *  @discussion Frame customization background color.
 *  @param color of background color.
 */
- (void)setFrameCustomizationBackgroundColor:(UIColor*)color;

/*!
 *  @discussion Cancel button location.
 *  @param loc enum.
 */
- (void)setCancelButtonLocation:(CancelBtnLoc)loc;

/*!
 *  @discussion Guidance button background color.
 *  @param color of background color.
 */
- (void)setGuidanceButtonBackgroundColor:(UIColor*)color;

/*!
 *  @discussion Guidance button disabled background color.
 *  @param color of background color.
 */
- (void)setGuidanceButtonDisabledBackgroundColor:(UIColor*)color;

/*!
 *  @discussion Guidance button highlight background color.
 *  @param color of background color.
 */

- (void)setGuidanceButtonHighlightBackgroundColor:(UIColor*)color;

/*!
 *  @discussion Guidance button border color.
 *  @param color of border color.
 */
- (void)setGuidanceButtonBorderColor:(UIColor*)color;

/*!
 *  @discussion Guidance ready header text color.
 *  @param color of text color.
 */
- (void)setGuidanceReadyScreenHeaderTextColor:(UIColor*)color;

/*!
 *  @discussion Guidance ready header sub text color.
 *  @param color of sub text color.
 */
- (void)setGuidanceReadyScreenHeaderSubTextColor:(UIColor*)color;

/*!
 *  @discussion Frame customization border color.
 *  @param color of border color.
 */
- (void)setFrameCustomizationBorderColor:(UIColor*)color;

/*!
 *  @discussion Frame customization stroke color.
 *  @param color of stroke color.
 */
- (void)setFrameCustomizationStrokeColor:(UIColor*)color;

/*!
 *  @discussion Result screen foreground  color.
 *  @param color of stroke color.
 */
- (void)setResultScreenForegroundColor:(UIColor*)color;

/*!
 *  @discussion Result screen activity indicator  color.
 *  @param color of stroke color.
 */
- (void)setResultActivityIndicatorColor:(UIColor*)color;

/*!
 *  @discussion Result screen upload progress fill color.
 *  @param color of stroke color.
 */
- (void)setResultScreenUploadProgressFillColor:(UIColor*)color;

/*!
 *  @discussion Result screen upload progress track color.
 *  @param color of stroke color.
 */
- (void)setResultScreenUploadProgressTrackColor:(UIColor*)color;

/*!
 *  @discussion Result screen animation background color.
 *  @param color of stroke color.
 */
- (void)setResultScreenAnimationBackgroundColor:(UIColor*)color;

/*!
 *  @discussion Result screen animation foreground color.
 *  @param color of stroke color.
 */
- (void)setResultScreenAnimationForegroundColor:(UIColor*)color;

/*!
 *  @discussion Screens frame border width.
 *  @param value of width.
 */
- (void)setBorderWidth:(int)value;

/*!
 *  @discussion Text fonts.
 *  @param font of text.
 */
- (void)setFont:(UIFont*)font;

/*!
 *  @discussion Liveness token string.
 *  @param livenessToken is the token to init liveness.
 */
- (void)setLivenessToken:(NSString*)livenessToken;

/*!
 *  @discussion Public key string.
 *  @param publicKey is the key to init liveness.
 */
- (void)setPublicKey:(NSString*)publicKey;

/*!
 *  @discussion App bundle id string.
 *  @param appId is the app bundle id.
 */
- (void)setAppBundleId:(NSString*)appId;

/*!
 *  @discussion Expiry Date.
 *  @param expiryDate is the date.
 */
- (void)setExpiryDate:(NSString*)expiryDate;

/*!
 *  @discussion Key id string.
 *  @param key value.
 */
- (void)setKey:(NSString*)key;

@end
