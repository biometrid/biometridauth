//
//  BiometridAuthEnums.h
//  BiometridAuth
//
//  Created by Tiago Carvalho on 29/12/2017.
//  Copyright © 2017 Tiago Carvalho. All rights reserved.
//

#ifndef BiometridAuthEnums_h
#define BiometridAuthEnums_h

/*!
 * @typedef EngineType
 * @brief The type of engine being used.
 * @constant FACE Referring to face engine.
 * @constant VOICE Referring to voice engine.
 * @constant DOCUMENT Referring to document engine.
 */
typedef NS_ENUM(NSInteger, EngineType) {
    
    FACE = 1,
    VOICE,
    DOCUMENT
};

/*!
 * @typedef SessionType
 * @brief The type of session being used.
 * @constant ADAPT Session to modify or adjust the user enroll.
 * @constant ENROLL Session to register the user.
 * @constant VERIFY Session to authenticate the user.
 * @constant COMPARE Session to compare the user document and selfie.
 */
typedef NS_ENUM(NSInteger, SessionType) {
    
    ADAPT = 1,
    ENROLL,
    VERIFY,
    COMPARE
};

/*!
 * @typedef Language
 * @brief Language being used.
 * @constant ENGLISH Referring to english language.
 * @constant SPANISH Referring to spanish language.
 * @constant PORTUGUESE Referring to portuguese language.
 */
typedef NS_ENUM(NSInteger, Language) {
    
    ENGLISH = 1,
    SPANISH,
    PORTUGUESE
};

/*!
 * @typedef CancelBtnLoc
 * @brief Button locations available.
 * @constant LEFT Cancel Button will be at left side.
 * @constant RIGHT Cancel Button will be at right side.
 * @constant DISABLED Cancel Button will dissapears.
 */
typedef NS_ENUM(NSUInteger, CancelBtnLoc) {
    
    LEFT,
    RIGHT,
    DISABLED
};

/*!
 * @typedef FaceResponseStatus
 * @brief Face engine response status.
 * @constant FaceSessionCompletedWithSuccess Face session was completed successfully.
 * @constant FaceSessionFailed Face session was completed unsuccessfully.
 * @constant FaceAlreadyDetectedOnGallery This face couldn't be enrolled because it already exists on the gallery associated to another user.
 * @constant FaceAlreadyEnrolled This user already has his face enrolled.
 * @constant FaceNotEnrolled This user doesn't have a face enroll.
 * @constant NoFaceEngineConfigurationAvailable Face engine is not available for this client.
 * @constant FaceRequestLimitReached The face engine's number of requests have reached it's limit.
 * @constant FaceEngineProblemOcurred Some problem ocurred, check error for more information.
 */
typedef NS_ENUM(NSInteger, FaceResponseStatus) {
    
    FaceSessionCompletedWithSuccess = 1,
    FaceSessionFailed,
    FaceAlreadyDetectedOnGallery,
    FaceAlreadyEnrolled,
    FaceNotEnrolled,
    NoFaceEngineConfigurationAvailable,
    FaceRequestLimitReached,
    FaceEngineProblemOcurred
    
};

/*!
 * @typedef VoiceResponseStatus
 * @brief Voice engine response status.
 * @constant VoiceSessionCompletedWithSuccess Voice session was completed successfully.
 * @constant DialogueStartedWithSuccess Voice session started successfully.
 * @constant AudioSubmittedWithSuccess Voice audio was submitted successfully.
 * @constant VoiceSessionFailed Voice session was completed unsuccessfully.
 * @constant VoiceAlreadyEnrolled This user already has his voice enrolled.
 * @constant VoiceNotEnrolled This user doesn't have a voice enroll.
 * @constant NoVoiceEngineConfigurationAvailable Voice engine is not available for this client.
 * @constant VoiceRequestLimitReached The voice engine's number of request have reached it's limit.
 * @constant VoiceEngineProblemOcurred Some problem ocurred, check error for more information.
 */
typedef NS_ENUM(NSInteger, VoiceResponseStatus) {
    
    VoiceSessionCompletedWithSuccess = 1,
    DialogueStartedWithSuccess,
    AudioSubmittedWithSuccess,
    VoiceSessionFailed,
    VoiceAlreadyEnrolled,
    VoiceNotEnrolled,
    NoVoiceEngineConfigurationAvailable,
    VoiceRequestLimitReached,
    VoiceEngineProblemOcurred
    
};

/*!
* @typedef LivenessResultStatus
* @brief Liveness result status.
* @constant LivenessSessionCompletedSuccessfully Session was performed successfully and a FaceMap was generated.
* @constant LivenessSessionUnsuccessful Session was not performed successfully and a FaceMap was not generated.  In general, other statuses will be sent to the developer for specific unsuccess reasons.
* @constant LivenessUserCancelled The user pressed the cancel button and did not complete the Session.
* @constant LivenessNonProductionModeLicenseInvalid Status will never be returned in a properly configured or production app, status is returned if your license is invalid or network connectivity issues occur during a session when the application is not in production.
* @constant LivenessCameraPermissionDenied The camera access is prevented because either the user has explicitly denied permission or the user's device is configured to not allow access by a device policy.
* @constant LivenessContextSwitch Session was cancelled due to the app being terminated, put to sleep, an OS notification, or the app was placed in the background.
* @constant LivenessLandscapeModeNotAllowed Session was cancelled because device is in landscape mode.
* @constant LivenessReversePortraitNotAllowed Session was cancelled because device is in reverse portrait mode.
* @constant LivenessTimeout Session was cancelled because the user was unable to complete Session in the default allotted time or the timeout set by the developer.
* @constant LivenessLowMemory Session was cancelled due to memory pressure.
* @constant LivenessNonProductionModeNetworkRequired Session was cancelled because your App is not in production and requires a network connection.
* @constant LivenessGracePeriodExceeded Session was cancelled because your License needs to be validated again.
* @constant LivenessMissingGuidanceImages Session was cancelled because not all guidance images were configured.
* @constant LivenessEncryptionKeyInvalid Session was cancelled because the developer-configured encryption key was not valid.
* @constant LivenessCameraInitializationIssue Session was cancelled because Liveness was unable to start the camera on this device.
* @constant LivenessLockedOut Session was cancelled because the user was in a locked out state.
* @constant LivenessUnknownInternalError Session was cancelled because of an unknown and unexpected error.
* @constant LivenessUserCancelledViaClickableReadyScreenSubtext Session cancelled because user pressed the Get Ready screen subtext message.
*/
typedef NS_ENUM(NSInteger, LivenessResultStatus) {
    LivenessSessionCompletedSuccessfully = 1,
    LivenessSessionUnsuccessful,
    LivenessUserCancelled,
    LivenessNonProductionModeLicenseInvalid,
    LivenessCameraPermissionDenied,
    LivenessContextSwitch,
    LivenessLandscapeModeNotAllowed,
    LivenessReversePortraitNotAllowed,
    LivenessTimeout,
    LivenessLowMemory,
    LivenessNonProductionModeNetworkRequired,
    LivenessGracePeriodExceeded,
    LivenessEncryptionKeyInvalid,
    LivenessMissingGuidanceImages,
    LivenessCameraInitializationIssue,
    LivenessLockedOut,
    LivenessUnknownInternalError,
    LivenessUserCancelledViaClickableReadyScreenSubtext,
    NoLiveness
};



#endif /* BiometridAuthEnums_h */
