//
//  BiometridAuthCamera.h
//  BiometridAuth
//
//  Created by Tiago Carvalho on 29/12/2017.
//  Copyright © 2017 Tiago Carvalho. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
#import <UIKit/UIKit.h>

@protocol BiometridAuthCameraDelegate

@optional
-(void)output:(UIImage*)image;

@end

@interface BiometridAuthCamera : NSObject

@property AVCaptureDevicePosition position;
@property UIView *view;

/*!
 *  @discussion Start camera.
 *  @param position AVCaptureDevicePosition of the camera (front/back).
 *  @param view UIView where camera is launched.
 */
+ (instancetype)startWithPosition:(AVCaptureDevicePosition)position inView:(UIView*)view;

/*!
 *  @discussion Take a photo.
 *  @param delegate BiometridAuthCameraDelegate.
 *  @param cropped to use just center image.
 */
- (void)takePhotoWithDelegate:(id<BiometridAuthCameraDelegate>)delegate cropped:(BOOL)cropped;

/*!
 *  @discussion Take a photo.
 *  @param delegate AVCapturePhotoCaptureDelegate..
 */
- (void)takeSelfiePhotoWithDelegate:(id<AVCapturePhotoCaptureDelegate>)delegate;

/*!
 *  @discussion Stop camera.
 */
- (void)stop;

/*!
 *  @discussion Load a photo from gallery.
 *  @param delegate BiometridAuthCameraDelegate.
 *  @param controller to show gallery view.
 *  @param cropped to use just center image.
 */
- (void)galleryPhotoWithDelegate:(id<BiometridAuthCameraDelegate>)delegate controller:(UIViewController*)controller cropped:(BOOL)cropped;

@end


