//
//  BiometridAuth.h
//  BiometridAuth
//
//  Created by Tiago Carvalho on 29/12/2017.
//  Copyright © 2017 Tiago Carvalho. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <BiometridAuth/BiometridAuthSDK.h>
#import <BiometridAuth/BiometridAuthEnums.h>
#import <BiometridAuth/BiometridAuthHelper.h>
#import <BiometridAuth/BiometridAuthManager.h>
#import <BiometridAuth/BiometridAuthCamera.h>
#import <BiometridAuth/BiometridAuthVoice.h>
#import <BiometridAuth/BiometridAuthFace.h>
#import <BiometridAuth/BiometridAuthDocument.h>
#import <BiometridAuth/BiometridAuthEngine.h>
#import <BiometridAuth/BiometridAuthFaceResponse.h>
#import <BiometridAuth/BiometridAuthVoiceResponse.h>
#import <BiometridAuth/BiometridAuthLivenessCustomization.h>
#import <BiometridAuth/NetworkSecuritySettings.h>

//! Project version number for BiometridAuth.
FOUNDATION_EXPORT double BiometridAuthVersionNumber;

//! Project version string for BiometridAuth.
FOUNDATION_EXPORT const unsigned char BiometridAuthVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <BiometridAuth/PublicHeader.h>


