//
//  BiometridAuthManager.h
//  BiometridAuth
//
//  Created by Tiago Carvalho on 29/12/2017.
//  Copyright © 2017 Tiago Carvalho. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <BiometridAuth/BiometridAuth.h>
#import <BiometridAuth/NetworkSecuritySettings.h>

@interface BiometridAuthManager : NSObject <NSURLSessionDelegate, NSURLSessionTaskDelegate>

@property (nonatomic, weak) id delegate;
@property NSString *processId;

+ (instancetype)sharedInstance;

/*!
 *  @discussion Creates a user.
 *  @param referenceId Client user's identifier on client's database.
 *  @param appToken Device specific token to send push notifications.
 */
- (void)createUser:(NSString*)referenceId withAppToken:(NSString*)appToken andCallback:(void(^)(NSDictionary *response, NSString *baUserId, NSError *error))completion;

/*!
 *  @discussion Updates a user's app token.
 *  @param userId Biometrid's user identifier.
 *  @param appToken Device specific token to send push notifications.
 */
- (void)updateUser:(NSString*)userId withAppToken:(NSString*)appToken andCallback:(void(^)(NSDictionary *response, BOOL isUpdated, NSError *error))completion;

/*!
 *  @discussion Deletes a user.
 *  @param userId Biometrid's user identifier.
 */
- (void)deleteUser:(NSString*)userId andCallback:(void(^)(NSDictionary *response, BOOL isDeleted, NSError *error))completion;

/*!
 *  @discussion Get a user's enroll status in the biometric engines.
 *  @param userId Biometrid's user identifier.
 */
- (void)getUserEnrollStatus:(NSString*)userId andCallback:(void(^)(NSDictionary *response, NSArray *baEngines, NSError *error))completion;

/*!
 *  @discussion Get a user's process flow.
 *  @param userId Biometrid's user identifier.
 */
- (void)getUserProcessFlow:(NSString*)userId andCallback:(void(^)(NSDictionary *response, EngineType engineType, NSError *error))completion;

/*!
 *  @discussion Get a user's voice engine languages
 *  @param userId Biometrid's user identifier.
 */
- (void)getVoiceEngineLanguages:(NSString*)userId andCallback:(void(^)(NSDictionary *response, NSArray *baLanguages, NSError *error))completion;

/*!
 *  @discussion Deletes a user's engine enroll. (if voice engine enroll, language must be chosen)
 *  @param userId Biometrid's user identifier.
 *  @param engine Engine's enroll to be deleted.
 *  @param language Voice enroll language to be deleted (en/es).
 */
- (void)deleteUserEnroll:(NSString*)userId forEngine:(EngineType)engine withLanguage:(Language)language andCallback:(void(^)(NSDictionary *response, BOOL isDeleted, NSError *error))completion;

/*!
 *  @discussion Cancel a pending authentication process.
 *  @param userId Biometrid's user identifier.
 */
- (void)cancelProcess:(NSString*)userId andCallback:(void(^)(NSDictionary *response, BOOL isCanceled, NSError *error))completion;

/*!
 *  @discussion Finish user's Biometrid session.
 *  @param userId Biometrid's user identifier.
 */
- (void)restartState:(NSString*)userId andCallback:(void(^)(NSDictionary *response, BOOL isRestarted, NSError *error))completion;

@end

