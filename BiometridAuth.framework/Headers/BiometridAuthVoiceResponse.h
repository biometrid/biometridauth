//
//  VoiceResponse.h
//  BiometridAuth
//
//  Created by Tiago Carvalho on 14/06/2018.
//  Copyright © 2018 Tiago Carvalho. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <BiometridAuth/BiometridAuthEnums.h>

@interface BiometridAuthVoiceResponse : NSObject

@property VoiceResponseStatus voiceResponseStatus;
@property NSString* promptCode;
@property NSString* promptCodeText;
@property NSString* decision;
@property NSString* dialogueStatus;
@property NSString* reason;
@property NSError *error;

+ (instancetype)voiceResponseWithStatus:(VoiceResponseStatus)voiceResponseStatus andError:(NSError*)error;

+ (instancetype)voiceResponseWithStatus:(VoiceResponseStatus)voiceResponseStatus withPromptCode:(NSString*)promptCode withPromptCodeText:(NSString*)promptCodeText andError:(NSError*)error;

@end

