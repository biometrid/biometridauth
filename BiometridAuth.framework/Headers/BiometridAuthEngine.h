//
//  BiometridAuthEngine.h
//  BiometridAuth
//
//  Created by Tiago Carvalho on 18/07/2018.
//  Copyright © 2018 Tiago Carvalho. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <BiometridAuth/BiometridAuthEnums.h>

@interface BiometridAuthEngine : NSObject

@property NSString *oid;
@property SessionType sessionType;
@property EngineType engineType;
@property Language language;

+ (instancetype)engineWithOid:(NSString*)oid withSessionType:(SessionType)sessionType withEngineType:(EngineType)engineType withLanguage:(Language)language;

@end
