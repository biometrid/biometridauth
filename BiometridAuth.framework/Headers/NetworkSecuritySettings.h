//
//  NetworkSettings.h
//  BiometridAuth
//
//  Created by André Santana on 01/06/2020.
//  Copyright © 2020 Polygon. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NetworkSecuritySettings : NSObject

+ (instancetype)sharedInstance;

- (void)setHostName:(NSString*)hostname;

- (void)setPins:(NSArray*)pins;

-(NSString*)getHostname;

-(NSArray*)getPings;

@end

NS_ASSUME_NONNULL_END
